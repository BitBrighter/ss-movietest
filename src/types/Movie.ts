interface Movie {
	title: string,
	poster: string,
	video: string,
	/*data: {
		title: string,
		poster: string,
		video: string,
	}*/
}

export default Movie;